import OktaAuth from '@okta/okta-auth-js'

const org = 'https://dev-933017.okta.com',
      clientId = '0oah47qbo1R4tOyb2356',
      redirectUri = 'https://localhost:5001',
      authorizationServer = 'default'

const oktaAuthClient = new OktaAuth({
  url: org,
  issuer: authorizationServer,
  clientId,
  redirectUri
})

export default {
  client: oktaAuthClient
}
